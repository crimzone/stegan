#!/usr/bin/env python3

import sys
import unittest


class Stegan:

    __def_order = ''.join([str(c) for c in range(1, 9)])  # default order '12345678'

    @staticmethod
    def __cycle_(order, ofs=0):
        """
        change the string, reorder it by at least 1 character
        e.g.
        abcd -> bcda
        :param order: the string to rotate
        :param ofs: additional rotating count
        :return: the rotated string
        """
        return order[-1-ofs:] + order[:-1-ofs]

    @staticmethod
    def __make_char_bit_preset_map(c, order):
        """
        make the map of flags for each bit that is turned in the character 'c' according to the bit order in 'order'
        :param c: single character
        :param order: string representing the order (zero based)
        :return: list of dictionaries representing the bit breakdown of c
        """
        p = {order.index(x): {'pos': int(x), 'word': '', 'val': 0} for x in order}
        p = sorted([p[x] for x in p], key=lambda x: x == 'pos')
        t = [int(bin(1 << i['pos']), 2) & ord(c) for i in p]
        for i,j in zip(p, t):
            i['val'] = j
        return p

    @staticmethod
    def __make_char_from_preset_map(sarr, order, ofs=0):
        """
        make the preset map according to order and fill it from a specific letter in each word in sarr
        then sum the values to get the integer representation of the char
        :param sarr: 8 words list
        :param order: string representing the order (zero based)
        :param ofs: the letter offset in the word from which to calculate
        :return: char
        """
        mp = {order.index(x): {'pos': int(x), 'val': 0} for x in order}
        mp = sorted([mp[x] for x in mp], key=lambda x: x=='pos')
        parr = [Stegan.__make_char_bit_preset_map(x[ofs], order) for x in sarr]
        for i, j in zip(mp, [x['pos'] for x in mp]):
            i['val'] = parr[j][j]['val']

        return chr(sum(i['val'] for i in mp))

    @staticmethod
    def steg(org, word_pool, **kw):
        """
        create a text from a word pool according to breakdown of each character in the original text
        :param org: text string to steg
        :param word_pool: list of strings or text string: the word pool from which we will build our new story
        :param order: order of the bits to use to build the new word set
        :param ofs: offset of the letter in word from word pool
        :return:
        """
        ofs = int(kw.get('ofs', 0))
        order = kw.get('order', Stegan.__def_order)

        order = ''.join([str(int(x)-1) for x in order])  # rebase to zero base
        if isinstance(word_pool, str):
            word_pool = word_pool.split(' ')
        from random import randint
        j = randint(0, len(org) + ofs)  # random seed ...
        jmax = len(word_pool)
        j %= jmax
        ofs %= 8
        story = []

        for i in org:
            c_set = Stegan.__make_char_bit_preset_map(i, order)
            k = 0
            while k < len(c_set):
                if len(word_pool[j]) > ofs:
                    t_set = Stegan.__make_char_bit_preset_map(word_pool[j][ofs], order)
                    if c_set[k]['val'] == t_set[k]['val']:
                        c_set[k]['word'] = word_pool[j]
                        k += 1
                j += randint(1, 42 + ofs)
                j %= jmax

            story += [c['word'] for c in c_set]
            Stegan.__cycle_(order, ofs)

        return ' '.join(story)

    @staticmethod
    def unsteg(text, dummy=None, **kw):
        """
        restructure the original text from a given text
        :param text: list of words or text string: the steg text we want to get our original text from
        :param order: order of the bits to use to unfold the original text
        :param ofs: offset of the letter in word from word pool
        :return: a string
        """
        ofs = int(kw.get('ofs', 0))
        order = kw.get('order', Stegan.__def_order)

        story = []
        ofs %= 8

        order = ''.join([str(int(x)-1) for x in order])  # rebase to zero base
        if isinstance(text, str):
            text = text.split(' ')
        if len(text) % 8:
            raise BufferError(len(text))

        unstegs = [text[i:i+8] for i in range(0, len(text), 8)]

        for i in unstegs:
            story += Stegan.__make_char_from_preset_map(i, order, ofs)
            Stegan.__cycle_(order, ofs)

        return ''.join(story)


"""
export Stegan methods
"""
steg = Stegan.steg
unsteg = Stegan.unsteg


class TestSteg(unittest.TestCase):
    """
    run tests using:
    python3 -m unittest stegan.TestSteg

    """
    words = None
    @classmethod
    def setUpClass(cls):
        with open('novel.txt') as f:
            cls.words = f.read()[:-1]

    @classmethod
    def tearDownClass(cls):
        cls.words = None

    def test_default_order_ofs(self):
        org = 'howdy quazimodo, please call igor and inform him to show up at doctor frankenstien lab'
        a = steg(org, self.words)
        b = unsteg(a)
        self.assertEqual(org, b)

    def test_non_default_order_ofs(self):
        org = 'the quick brown fox jumps over the lazy dog'
        a = steg(org, self.words, order='12345678', ofs=44)
        b = unsteg(a, order='12345678', ofs=44)

        self.assertEqual(org, b)


if __name__ == "__main__":
    """
    python3 stegan.py novel.txt something.txt  4  65748312 > z
    python3 stegan.py un z 4  65748312
    """
    argc = len(sys.argv)
    if argc < 3:
        sys.exit("missing argument")

    if 'un' in sys.argv[1]:
        do = unsteg
        wp = sys.argv[1]
    else:
        do = steg
        wp = open(sys.argv[1]).read()[:-1]

    txt = open(sys.argv[2]).read()[:-1]

    try:
        if argc == 5:
            xt = do(txt, wp, order=sys.argv[4], ofs=sys.argv[3])
        elif argc == 4:
            xt = do(txt, wp, ofs=sys.argv[3])
        else:
            xt = do(txt, wp)
        print(xt)
    except Exception as e:
        print("error occurred [%r]" % e)



